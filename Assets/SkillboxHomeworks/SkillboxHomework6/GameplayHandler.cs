using UnityEngine;
using UnityEngine.UI;

public class GameplayHandler : MonoBehaviour
{
    public int leftPin;
    public int midPin;
    public int rightPin;
    
    public Text leftPinText;
    public Text midPinText;
    public Text rightPinText;

    public GameObject winScreen;
    
    
    private void Start()
    {
        TextUpdate();
    }

    private void Update()
    {
        if (leftPin == 5 && midPin == 5 && rightPin == 5)
        {
            winScreen.gameObject.SetActive(true);  
        }
    }

    public void TextUpdate()
    {
        leftPinText.text = leftPin.ToString();
        midPinText.text = midPin.ToString();
        rightPinText.text = rightPin.ToString();
    }

    public void OnDrelButtonClick()
    {
        leftPin++;
        midPin--;
        TextUpdate();
    }
    
    public void OnMolotokButtonClick()
    {
        leftPin--;
        midPin = midPin + 2;
        rightPin--;
        TextUpdate();
    }
    
    public void OnOtmichkaButtonClick()
    {
        leftPin--;
        midPin++;
        rightPin++;
        TextUpdate();
    }

}
