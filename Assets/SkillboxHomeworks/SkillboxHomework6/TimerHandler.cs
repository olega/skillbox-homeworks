using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerHandler : MonoBehaviour
{
    public GameObject loseScreen;

    public float time;
    public Text timerText;

    private float _timeLeft;
    
    public void OnButtonStartClick()
    {
        _timeLeft = time;
        StartCoroutine(StartTimer());
    }
    
    private IEnumerator StartTimer()
    {
        while (_timeLeft > 0)
        {
            _timeLeft -= Time.deltaTime;
            UpdateTimerText();
            yield return null;
        }
    }

    private void UpdateTimerText()
    {
        if (_timeLeft < 0)
        {
            _timeLeft = 0;
            loseScreen.gameObject.SetActive(true);
        }

        timerText.text = (Mathf.FloorToInt(_timeLeft)).ToString();
    }
    
    
}
