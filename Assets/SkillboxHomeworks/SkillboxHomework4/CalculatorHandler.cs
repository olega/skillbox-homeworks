using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalculatorHandler : MonoBehaviour
{
    [SerializeField] private InputField inputField1;
    [SerializeField] private InputField inputField2;
    [SerializeField] private Text answerText;
    [SerializeField] private Text selectedOperandCharText;


    public void OnPlusButtonClicked()
    {
        var answer = Convert.ToInt32(inputField1.text) + Convert.ToInt32(inputField2.text);
        answerText.text = answer.ToString();
        selectedOperandCharText.text = "+";
    }
    
    public void OnMinusButtonClicked()
    {
        var answer = Convert.ToInt32(inputField1.text) - Convert.ToInt32(inputField2.text);
        answerText.text = answer.ToString();
        selectedOperandCharText.text = "-";
    }
    
    public void OnMultiplyButtonClicked()
    {
        var answer = Convert.ToInt32(inputField1.text) * Convert.ToInt32(inputField2.text);
        answerText.text = answer.ToString();
        selectedOperandCharText.text = "*";
    }
    
    public void OnDivideButtonClicked()
    {
        var answer = Convert.ToInt32(inputField1.text) / Convert.ToInt32(inputField2.text);
        answerText.text = answer.ToString();
        selectedOperandCharText.text = "/";
    }
}
