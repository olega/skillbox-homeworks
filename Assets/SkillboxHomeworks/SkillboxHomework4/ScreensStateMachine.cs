using UnityEngine;

public class ScreensStateMachine : MonoBehaviour
{
    [SerializeField] private GameObject emptyScreen;
    [SerializeField] private GameObject calculatorScreen;
    [SerializeField] private GameObject compareTwoNumbersScreen;
    [SerializeField] private GameObject compareThreeNumbersScreen;
    [SerializeField] private GameObject quadraticEquationScreen;

    private GameObject currentScreen;
    
    private void Start()
    {
        emptyScreen.SetActive(true);
        currentScreen = emptyScreen;
    }

    public void ChangeState(GameObject state)
    {
        if (currentScreen != null)
        {
            currentScreen.SetActive(false);
            state.SetActive(true);
            currentScreen = state;
        }
    }
}
