using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompareTwoNumbersHandler : MonoBehaviour
{
    [SerializeField] private InputField firstOperand;
    [SerializeField] private InputField secondOperand;
    [SerializeField] private Text answerText;

    public void CompareTwoNumbers()
    {
        int intFirstOperand = Convert.ToInt32(firstOperand.text);
        int intSecondOperand = Convert.ToInt32(secondOperand.text);

        if (intFirstOperand > intSecondOperand)
        {
            answerText.text = intFirstOperand.ToString();
        }
        else if (intFirstOperand < intSecondOperand)
        {
            answerText.text = intSecondOperand.ToString();
        }
        else if (intFirstOperand == intSecondOperand)
        {
            answerText.text = "Числа равны";
        }
    }
    
}
