using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolveQuadraticEquationHandler : MonoBehaviour
{
    [SerializeField] private InputField firstOperand;
    [SerializeField] private InputField secondOperand;
    [SerializeField] private InputField thirdOperand;
    [SerializeField] private Text answerText;
    [SerializeField] private Text discriminantText;


    public void SolveQuadraticEquation()
    {
        
        double a = Convert.ToDouble(firstOperand.text);
        double b = Convert.ToDouble(secondOperand.text);
        double c = Convert.ToDouble(thirdOperand.text);

        double d = b * b - 4 * a * c;
        discriminantText.text = d.ToString();

        if (d < 0)
        {
            answerText.text = "Так как дискриминант меньше нуля то корней у выражения нет";
        }
        if (d == 0)
        {
            double x = -b / ( 2 * a );
            answerText.text = "Так как дискриминант равен нулю то корень у выражения один х=" + x + ".";
        }
        if (d > 0)
        {
            double x1 = (-b - Math.Sqrt(d)) / (2 * a);
            double x2 = (-b + Math.Sqrt(d)) / (2 * a);
            answerText.text = "Так как дискриминант больше нуля то кореней у выражения два х1=" + x1 + ", х2=" + x2 + ".";
        }
    }

}
