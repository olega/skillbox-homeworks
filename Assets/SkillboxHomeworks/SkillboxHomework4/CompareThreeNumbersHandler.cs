using System;
using UnityEngine;
using UnityEngine.UI;

public class CompareThreeNumbersHandler : MonoBehaviour
{
    [SerializeField] private InputField firstOperand;
    [SerializeField] private InputField secondOperand;
    [SerializeField] private InputField thirdOperand;
    [SerializeField] private Text answerText;

    public void CompareThreeNumbers()
    {
        int intFirstOperand = Convert.ToInt32(firstOperand.text);
        int intSecondOperand = Convert.ToInt32(secondOperand.text);
        int intThirdOperand = Convert.ToInt32(thirdOperand.text);

        if (intFirstOperand < intSecondOperand && intFirstOperand < intThirdOperand)
        {
            answerText.text = intSecondOperand.ToString() + " и " + intThirdOperand.ToString();
        }
        else if (intSecondOperand < intFirstOperand && intSecondOperand < intThirdOperand)
        {
            answerText.text = intFirstOperand.ToString() + " и " + intThirdOperand.ToString();
        }
        else if (intThirdOperand < intFirstOperand && intThirdOperand < intSecondOperand)
        {
            answerText.text = intFirstOperand.ToString() + " и " + intSecondOperand.ToString();
        }
        else if (intFirstOperand == intSecondOperand && intFirstOperand == intThirdOperand)
        {
            answerText.text = "Числа равны";
        }
    }
}
