using UnityEngine;

namespace SkillboxHomeworks.SkillboxHomework8.Homework8_Task2
{
    public class RelayRaceMovement : MonoBehaviour
    {
        public Transform[] _runners; // Массив бегунов
        public float _speed; // Скорость движения бегунов
        public float _passDistance; // Расстояние для передачи эстафеты

        private int currentRunnerIndex = 0; // Текущий бегун

        private void Update()
        {
            MoveRUnner();
        }

        private void MoveRUnner()
        {
            if (_runners.Length == 0)
            {
                return;
            }

            Transform currentRunner = _runners[currentRunnerIndex];
            Transform nextRunner = _runners[(currentRunnerIndex + 1) % _runners.Length];
            
            // Повернуть текущего бегуна в сторону следующего бегуна
            Vector3 direction = (nextRunner.position - currentRunner.position).normalized;
            
            // Двигать текущего бегуна к следующему
            currentRunner.position =
                Vector3.MoveTowards(currentRunner.position, nextRunner.position, _speed * Time.deltaTime);
            
            // Проверить дистанцию до следующего бегуна
            if (Vector3.Distance(currentRunner.position, nextRunner.position) < _passDistance)
            {
                currentRunnerIndex = (currentRunnerIndex + 1) % _runners.Length;
            }
        }
    }
}
