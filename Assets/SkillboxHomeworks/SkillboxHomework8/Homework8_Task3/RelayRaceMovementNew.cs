using UnityEngine;

namespace SkillboxHomeworks.SkillboxHomework8.Homework8_Task3
{
    public class RelayRaceMovementNew : MonoBehaviour
    {
        public Transform[] runners; // Массив бегунов
        public float speed = 5f; // Скорость движения бегунов
        public float passDistance = 1f; // Расстояние для передачи эстафеты

        public Transform baton; // Ссылка на эстафетную палочку

        private int currentRunnerIndex = 0; // Текущий бегун

        public void Start()
        {
            if (runners.Length == 0)
            {
                Debug.LogError("No runners set for movement");
                enabled = false;
                return;
            }

            // Прикрепляем палочку к первому бегуну
            AttachBatonToRunner(runners[currentRunnerIndex]);
        }

        public void Update()
        {
            MoveRunner();
        }

        private void MoveRunner()
        {
            if (runners.Length == 0)
                return;

            Transform currentRunner = runners[currentRunnerIndex];
            Transform nextRunner = runners[(currentRunnerIndex + 1) % runners.Length];

            // Повернуть текущего бегуна в сторону следующего бегуна
            Vector3 direction = (nextRunner.position - currentRunner.position).normalized;
            currentRunner.rotation = Quaternion.LookRotation(direction);

            // Двигать текущего бегуна к следующему
            currentRunner.position = Vector3.MoveTowards(currentRunner.position, nextRunner.position, speed * Time.deltaTime);

            // Проверить дистанцию до следующего бегуна
            if (Vector3.Distance(currentRunner.position, nextRunner.position) < passDistance)
            {
                currentRunnerIndex = (currentRunnerIndex + 1) % runners.Length;
                AttachBatonToRunner(runners[currentRunnerIndex]);
            }
        }

        private void AttachBatonToRunner(Transform runner)
        {
            RunnerVisualizer runnerVisualizer = runner.GetComponent<RunnerVisualizer>();
            if (runnerVisualizer != null && baton != null)
            {
                runnerVisualizer.baton = baton;
                runnerVisualizer.AttachBaton();
            }
        }
    }
}