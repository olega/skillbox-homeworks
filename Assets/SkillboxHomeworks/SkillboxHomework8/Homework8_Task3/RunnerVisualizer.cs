using System;
using Unity.Mathematics;
using UnityEngine;

namespace SkillboxHomeworks.SkillboxHomework8.Homework8_Task3
{
    public class RunnerVisualizer : MonoBehaviour
    {
        public Transform baton;
        public Transform handPosition;

        private void Start()
        {
            AttachBaton();
        }

        public void AttachBaton()
        {
            if (baton != null && handPosition != null)
            {
                baton.SetParent(handPosition);
                baton.localPosition = Vector3.zero;
                baton.localRotation = Quaternion.identity;
                baton.localScale = Vector3.one; // Сброс масштаба
            }
        }
    }
}
