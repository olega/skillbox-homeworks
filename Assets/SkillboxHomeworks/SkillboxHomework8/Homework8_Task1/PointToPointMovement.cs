using UnityEngine;

namespace SkillboxHomeworks.SkillboxHomework8.Homework8_Task1
{
    public class PointToPointMovement : MonoBehaviour
    {
        public Vector3[] _points; // Массив точек движения
        public float _speed; // Скорость движения объекта

        private int currentIndex = 0; // Текущий индекс в массиве точек
        private bool forward = true; // Флаг для направления движения

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            // Если массив точек пустой, не выполнять никаких действий
            if (_points.Length == 0)
            {
                return;
            }
        
            // Получаем текущую цель
            Vector3 target = _points[currentIndex];
        
            // Двигаемся к текущей цели
            transform.position = Vector3.MoveTowards(transform.position, target, _speed * Time.deltaTime);
        
            // Проверяем, достигли ли мы текущей цели
            if (Vector3.Distance(transform.position, target) < 0.1f)
            {
                if (forward)
                {
                    currentIndex++;
                    if (currentIndex >= _points.Length - 1)
                    {
                        currentIndex = _points.Length - 1;
                        forward = false;
                    }
                }
                else
                {
                    currentIndex--;
                    if (currentIndex < 0)
                    {
                        currentIndex = 0;
                        forward = true;
                    }
                } 
            }
        }
    }
}
