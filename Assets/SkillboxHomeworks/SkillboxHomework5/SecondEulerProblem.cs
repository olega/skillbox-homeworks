using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SecondEulerProblem : MonoBehaviour
{
    public InputField origNumbField;

    public Text title;
    public Text answer;
    
    public void OnButtonCalculateClick()
    {
        var listFib = new List<int>();
        var listEven = new List<int>();
        
        int.TryParse(origNumbField.text, out int origNumb);
        
        int firstNumber = 1;
        int secondNumber = 1;
        int summ = 0;

        listFib.Add(firstNumber);
        listFib.Add(secondNumber);

        while (origNumb >= (firstNumber + secondNumber))
        {
            summ = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = summ;
            listFib.Add(summ);
        }
        
        foreach (var i in listFib)
        {
            if (i % 2 == 0)
            {
                listEven.Add(i);
            }
        }
        
        int sum = listEven.ToArray().Sum();
        title.text = $"Cумму всех четных элементов ряда Фибоначчи до {origNumb} включительно";
        answer.text = sum.ToString();
    }


}
