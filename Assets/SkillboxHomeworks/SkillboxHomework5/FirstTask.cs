using UnityEngine;
using UnityEngine.UI;

public class FirstTask : MonoBehaviour
{
    [SerializeField] private InputField fromValue;
    [SerializeField] private InputField toValue;
    [SerializeField] private GameObject cellPrefab;


    public void OnResultButtonClick()
    {
        if (int.TryParse(fromValue.text, out int from) && int.TryParse(toValue.text, out int to))
        {
            for (int i = from; i <= to; i++)
            {
                if (i % 2 == 0)
                {
                    var newCellPrefab = Instantiate(cellPrefab, transform);
                    
                    var newCellColor = cellPrefab.GetComponent<Image>();
                    newCellColor.color = Random.ColorHSV();
                
                    var newCellText = newCellPrefab.GetComponentInChildren<Text>();
                    newCellText.text = $"{i}";
                }
            }
        }
    }
}
