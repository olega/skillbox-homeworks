using UnityEngine;
using UnityEngine.UI;

public class SecondTask : MonoBehaviour
{
    public GameObject cellPrefab;

    public void OnCalculateButtonClick()
    {
        for (int i = 1; i <= 10; i++)
        {
            for (int j = 1; j < 10; j++)
            {
                var newCellPrefab = Instantiate(cellPrefab, transform);
                
                var newCellColor = cellPrefab.GetComponent<Image>();
                newCellColor.color = Random.ColorHSV();
                
                var newCellText = newCellPrefab.GetComponentInChildren<Text>();
                newCellText.text = $"{i} * {j} = " + (i * j);
            }
        }    
    }
}
