using UnityEngine;

public class ScreensStateMachineHW5 : MonoBehaviour
{
    [SerializeField] private GameObject emptyScreen;
    //[SerializeField] private GameObject mainScreen;
    [SerializeField] private GameObject factorialScreen;
    [SerializeField] private GameObject multScreen;
    [SerializeField] private GameObject evenScreen;
    [SerializeField] private GameObject firstEulerProblemScreen;
    [SerializeField] private GameObject secondEulerProblemScreen;
    

    private GameObject currentScreen;
    
    private void Start()
    {
        emptyScreen.SetActive(true);
        currentScreen = emptyScreen;
    }

    public void ChangeState(GameObject state)
    {
        if (currentScreen != null)
        {
            currentScreen.SetActive(false);
            state.SetActive(true);
            currentScreen = state;
        }
    }
}
