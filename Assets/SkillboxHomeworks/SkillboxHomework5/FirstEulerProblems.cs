using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FirstEulerProblems : MonoBehaviour
{
    public InputField origNumbField;
    public InputField firstOperandField;
    public InputField secondOperandField;

    public Text title;
    public Text answer;
    
    public void OnButtonCalculateClick()
    {
        int.TryParse(origNumbField.text, out int origNumb);
        int.TryParse(firstOperandField.text, out int firstOperand);
        int.TryParse(secondOperandField.text, out int secondOperand);

        
        var list = new List<int>();
        
        for (int i = 1; i < origNumb; i++)
        {
            if (i % firstOperand == 0 || i % secondOperand == 0)
            {
                list.Add(i);
            }
        }
        
        int sum = list.ToArray().Sum();

        title.text = $"Сумма чисел, которые деляться на {firstOperand} или {secondOperand} до {origNumb}";
        answer.text = sum.ToString();
    }
}
