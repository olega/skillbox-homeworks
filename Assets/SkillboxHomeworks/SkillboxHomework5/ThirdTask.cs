using UnityEngine;
using UnityEngine.UI;

public class ThirdTask : MonoBehaviour
{
    [SerializeField] private InputField number;
    [SerializeField] private Text result;

    
    private void Update()
    {
        if (int.TryParse(number.text, out int res))
        {
            int n = res;
            
            if (n >= 0)
            {
                result.text = $"Факториал числа {n}: {CalculateFactorialUsingCycle(n)}"; //Подставьте сюдя один из методов ниже (CalculateFactorialUsingRecursion ИЛИ CalculateFactorialUsingRecursion) чтобы высчитать факотриал разными способами (через рекурсию ИЛИ через простой цикл)
            }
            else
            {
                result.text ="У отрицательных чисел нет факториала";
            }
        }
    }

    private int CalculateFactorialUsingRecursion(int n) //Вычисление факоториала с помощью рекурсии
    {
        if (n == 0)
        {
            return 1;
        }
        else
        {
            return n * CalculateFactorialUsingRecursion(n - 1);
        }
    }
    
    private int CalculateFactorialUsingCycle(int n) //Вычисление факоториала с помощью цикла
    {
        int factotial = 1;

        if (n != 0)
        {
            for (int i = 2; i <= n; i++)
            {
                factotial *= i;
            }
        }
        return factotial;
    }
}
