# Репозиторий для домашних заданий от Skillbox


## Где находятся файлы для проверки:
>    - Все проекты с домашними заданиями разбиты по урокам в папке `/Assets/SkillboxHomeworks/`
>    - Перейдя в папку `/Assets/SkillboxHomeworks/` выберете урок для проверки, например `/Assets/SkillboxHomeworks/SkillboxHomework4`
>    - В папке будут все необходимые файлы для этого урока (префабы, скрипты, сцена (по возможности одна,если этого не требует задание))

## P.S.
>    - Прочитайте что выведет `код` ниже =)

```c
public class MyClass
{
    private string firstString;
    private string secondString;

    public void Start()
    {
       CompareStrings();
    }

    public void CompareStrings()
    {
        firstString = "Спасибо Вам ";
        secondString = "за Вашу работу =)";

        Debug.Log(firstString + secondString);
    }
}
```


## TODO:

- [x] Залить репозиторий
- [x] Создать забавный `ReadMe`
- [ ] Устроится на работу
- [ ] Создать хорошую игру, которой бы гордился
